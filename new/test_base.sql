-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 09 2017 г., 14:06
-- Версия сервера: 10.1.25-MariaDB
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `id_parent`, `name`) VALUES
(1, 0, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€Ñ‹'),
(2, 0, 'ÐœÑƒÐ»ÑŒÑ‚Ð¸Ð¼ÐµÐ´Ð¸Ð°'),
(3, 1, 'Ð–Ðš Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€Ñ‹'),
(4, 1, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ðµ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€Ñ‹'),
(5, 3, 'Ð”Ð¸Ð°Ð³Ð¾Ð½Ð°Ð»ÑŒÑŽ Ð´Ð¾ 45 Ð´ÑŽÐ¹Ð¼Ð¾Ð²'),
(6, 3, 'Ð”Ð¸Ð°Ð³Ð¾Ð½Ð°Ð»ÑŒÑŽ Ð±Ð¾Ð»ÐµÐµ 40 Ð´ÑŽÐ¹Ð¼Ð¾Ð²'),
(7, 4, 'Ð”Ð¸Ð°Ð³Ð¾Ð½Ð°Ð»ÑŒÑŽ Ð´Ð¾ 45 Ð´ÑŽÐ¹Ð¼Ð¾Ð²'),
(8, 4, 'Ð”Ð¸Ð°Ð³Ð¾Ð½Ð°Ð»ÑŒÑŽ Ð±Ð¾Ð»ÐµÐµ 40 Ð´ÑŽÐ¹Ð¼Ð¾Ð²'),
(9, 2, 'DVD-Ð¿Ð»ÐµÐµÑ€Ñ‹'),
(10, 2, 'Blu-Ray Ð¿Ð»ÐµÐµÑ€Ñ‹');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `id_group`, `name`) VALUES
(1, 9, 'DVD-Ð¿Ð»ÐµÐµÑ€ BBK DVP 753HD'),
(2, 9, 'DVD-Ð¿Ð»ÐµÐµÑ€ BBK DVP 953HD'),
(3, 9, 'DVD-Ð¿Ð»ÐµÐµÑ€ BBK DMP1024HD (+ 3 DVD Ð´Ð¸ÑÐºÐ°)'),
(4, 2, 'ÐœÐ°Ð³Ð½Ð¸Ñ‚Ð¾Ð»Ð° HYUNDAI H-1404'),
(5, 10, 'Blu-ray Ð¿Ð»ÐµÐµÑ€ PHILIPS DVP3700 (51)'),
(6, 8, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ LG 50PZ250 (3D)'),
(7, 8, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ Samsung PS51D450'),
(8, 7, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ LG 42PT250'),
(9, 7, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ LG 42PW451 (3D)'),
(10, 4, 'ÐŸÐ»Ð°Ð·Ð¼ÐµÐ½Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ LG 50PZ551 (3D)'),
(11, 5, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš LG 26LK330'),
(12, 5, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš Fusion FLTV-16W7'),
(13, 6, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš LG 42LK530'),
(14, 6, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš LG 42LK551'),
(15, 6, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš LG 47LK530'),
(16, 3, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€-Ð–Ðš Samsung LE32D403'),
(17, 1, 'Ð¢ÐµÐ»ÐµÐ²Ð¸Ð·Ð¾Ñ€ Erisson 1435');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
