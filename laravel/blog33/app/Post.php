<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
		public $timestamps = FALSE;
		
		public function comments(){
return $this->hasMany('App\Comment');//свяывается с несколькими моделями comment
}


public function author(){
		return $this->belongsTo('App\Author');//с одной моделью author
	}

}
