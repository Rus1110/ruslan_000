<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/qui', function () {
    return view('welcome');
});


Route::match(['get','post'],'/author',['uses'=>'AuthorController@index1','as'=>'cont']);
Route::match(['get','post'],'/post',['uses'=>'PostController@index2','as'=>'cont2']);
Route::match(['get','post'],'/comment',['uses'=>'CommentController@index3','as'=>'cont3']);
